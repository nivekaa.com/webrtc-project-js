//Ctrl+Alt+p

navigator.getUserMedia = navigator.getUserMedia ||
 navigator.webkitGetUserMedia || 
 navigator.mozGetUserMedia;


function bindEvents(p) {
    p.on("error", function (error) {
        alert(error);
    })
    p.on("signal", function (data) {
        document.querySelector("#offer").textContent = JSON.stringify(data);
    });

    p.on("stream", function (stream) {
        let video  = document.querySelector("#receiver-video")
        video.volume = 0
        video.srcObject = stream
        video.play();
    });

    document.querySelector("#incoming").addEventListener("submit", function (e) {
        e.preventDefault();
        p.signal(JSON.parse(e.target.querySelector("textarea").value));
        // 
    })
}

document.querySelector("#start-conf").addEventListener("click", function (e) {

    startPeer(true);
    
})


document.querySelector("#receive-conf").addEventListener("click", function (e) {

    startPeer(false);
    
})

function startPeer(initiator) {
    navigator.getUserMedia({
        video: true,
        audio: true
    }, function (stream) {
        let p = new SimplePeer({
            initiator: initiator,
            stream: stream,
            trickle: false,
            //config:
        })
        bindEvents(p);
        let emitterV  = document.querySelector("#emitter-video")
        emitterV.volume = 0
        emitterV.srcObject = stream
        emitterV.play();
    }, function () {
        
    })
}